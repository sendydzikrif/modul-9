public class Person
{
	protected String name;
	protected String address;

	public Person()
	{
		System.out.println("Inside Person : Constructor");
		name = "";
		address = "";
	}

	public Person(String name, String address)
	{
		this.name = name;
		this.address = address;
	}
	
	public String getName()
	{
		System.out.println("Person name : "+name);
		return name;
	}

	public String getAddress()
	{
		System.out.println("Person Address : "+address);
		return address;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setAddress(String add)
	{
		this.address = address;
	}
	
}