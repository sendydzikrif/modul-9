public class PrivateElevator2
{
	private boolean bukaPintu = false;
	private int lantaiSekarang = 1;
	private int berat = 0;

	private final int KAPASITAS = 1000;
	private final int LANTAI_ATAS = 5;
	private final int LANTAI_BAWAH = 1;

	public void buka()
	{
		bukaPintu = true;
	}

	public void tutup()
	{
		hitungKapasitas();
		if(berat <=KAPASITAS)
		{
			bukaPintu = false;
		}
		else
		{
			System.out.println("Elevator Kelebihan beban");
			System.out.println("Pintu akan tetap terbuka sampai seseorang keluar");
		}
	}

	private void hitungKapasitas()
	{
		berat = (int)(Math.random()*1500);
		System.out.println("Berat : "+ berat);
	}

	public void naik()
	{
		if(!bukaPintu)
		{
			if(lantaiSekarang < LANTAI_ATAS)
			{
				lantaiSekarang++;
				System.out.println(lantaiSekarang);
			}
			else
			{
				System.out.println("Sudah mencapai lantai atas");
			}
		}
		else
		{
			System.out.println("Pintu Masih Terbuka");
		}
	}

	public void turun()
	{
		if(!bukaPintu)
		{
			if(lantaiSekarang > LANTAI_BAWAH)
			{
				lantaiSekarang--;
				System.out.println(lantaiSekarang);
			}
			else
			{
				System.out.println("Sudah Mencapai Lantai Bawah");
			}
		}
		else
		{
			System.out.println("Pintu Masih Terbuka ");
		}
	}

	public void setLantai(int tujuan)
	{
		if((tujuan >= LANTAI_BAWAH)&&(tujuan <= LANTAI_ATAS))
		{
			while (lantaiSekarang != tujuan)
			{
				if(lantaiSekarang < tujuan)
				{
					naik();
				}
				else
				{
					turun();
				}
			}
		}
		else
		{
			System.out.println("Lantai Salah");
		}
	}

	public int getLantai()
	{
		return lantaiSekarang;
	}
	public boolean getStatusPintu()
	{
		return bukaPintu;
	}
}