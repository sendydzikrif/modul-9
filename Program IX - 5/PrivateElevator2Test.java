public class PrivateElevator2Test
{
	public static void main(String[] args)
	{
		PrivateElevator2 privateElevator = new PrivateElevator2();
		privateElevator.buka();
		privateElevator.tutup();
		privateElevator.turun();
		privateElevator.naik();
		privateElevator.naik();
		privateElevator.buka();
		privateElevator.tutup();
		privateElevator.turun();
		privateElevator.buka();
		privateElevator.turun();
		privateElevator.tutup();
		privateElevator.turun();
		privateElevator.turun();

		int lantai = privateElevator.getLantai();

		if(lantai !=5 && !privateElevator.getStatusPintu())
		{
			privateElevator.setLantai(5);
		}
		privateElevator.setLantai(10);
		privateElevator.buka();
	}
}