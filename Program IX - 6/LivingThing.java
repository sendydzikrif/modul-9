public abstract class LivingThing
{
	public void breath()
	{
		System.out.println("Living Thing Breathing...");
	}
	public void eat()
	{
		System.out.println("Living Thing Eating...");
	}

	public abstract void walk();
}